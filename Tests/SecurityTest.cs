﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using NUnit.Framework;
using server_http_dev;

namespace Tests
{
    [TestFixture]
    class SecurityTest
    {
        [Test]
        public void CheckingConfidentialPathTest()
        {
            Config.useDefaultSettings();

            Assert.IsTrue(Security.checkPath("../../../server_http_dev/shared/index.php"));
            Assert.IsTrue(Security.checkPath("../../../server_http_dev/shared/example"));
            Assert.IsFalse(Security.checkPath(@"D:/test.txt"));
        }

        [Test]
        public void FilteringIpAddressTest()
        {
            Config.useDefaultSettings();

            IPAddress ipFromList = IPAddress.Parse("127.0.0.1");
            IPAddress ipNotFromList = IPAddress.Parse("192.168.10.20");

            if (!Security.ipList.Contains(ipFromList))  Security.ipList.Add(ipFromList);
            if (Security.ipList.Contains(ipNotFromList)) Security.ipList.Remove(ipNotFromList);

            Config.setSetting("ip_filtering" , Security.FILTERING_NONE.ToString());
            Assert.IsTrue(Security.hasAccess(ipFromList));
            Assert.IsTrue(Security.hasAccess(ipNotFromList));

            Config.setSetting("ip_filtering", Security.FILTERING_BLACKLIST.ToString());
            Assert.IsFalse(Security.hasAccess(ipFromList));
            Assert.IsTrue(Security.hasAccess(ipNotFromList));

            Config.setSetting("ip_filtering", Security.FILTERING_WHITELIST.ToString());
            Assert.IsTrue(Security.hasAccess(ipFromList));
            Assert.IsFalse(Security.hasAccess(ipNotFromList));

        }
    }
}

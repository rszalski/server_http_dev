﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Schema;
using System.Threading.Tasks;

namespace server_http_dev
{
    public static class URLRewriter
    {
        public static Dictionary<string, string> rules;
        public static List<String> regularFile;

        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void init()
        {
            try
            {
                rules = null;
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.Schemas.Add(null, Config.schema_rules);
                settings.ValidationType = ValidationType.Schema;
                XmlDocument doc = new XmlDocument();
                doc.Load(Config.rewrite_rules);
                XmlReader rdr = XmlReader.Create(new StringReader(doc.InnerXml), settings);
                while (rdr.Read()) { }
                loadRules(doc.DocumentElement.ChildNodes);
            }
            catch(Exception e) {
                log.Error(e.Message);
            }
       }

        private static void loadRules(XmlNodeList xmlRules)
        {
            if (xmlRules.Count > 0)
            {
                XmlNode enabled = null;
                String source, destination;
                foreach (XmlNode rule in xmlRules)
                {
                    enabled = rule.Attributes.GetNamedItem("enabled");
                    if (enabled == null || enabled.Value.ToUpper().Equals("TRUE"))
                    {
                        if (rules == null)
                        {
                            rules = new Dictionary<string, string>();
                            regularFile = new List<string>();
                        }
                        source = rule.Attributes.GetNamedItem("source").Value;
                        destination = rule.Attributes.GetNamedItem("destination").Value;
                        if (rule.Attributes.GetNamedItem("regularFile") != null)
                        {
                            if (rule.Attributes.GetNamedItem("regularFile").Value.ToUpper().Equals("TRUE"))
                            {
                                regularFile.Add(source);
                            }
                        }
                        rules.Add(source, destination);
                    }
                }
            }
        }

        public static string rewrite(string data)
        {
            if ((rules != null) && (data.Length > 0))
            {
                string newData = null;
                foreach (string source in rules.Keys)
                {
                    Regex regex = new Regex(source, RegexOptions.IgnoreCase);
                    Match match = regex.Match(data);

                    if (match.Success)
                    {
                        newData = regex.Replace(data, rules[source].Replace('/', '\\'));
                        Console.WriteLine(newData);
                        if (regularFile.Contains(source))
                        {
                            if (File.Exists(Path.Combine(Config.serve_folder, data.Replace('/', '\\'))))
                            {
                                Console.WriteLine(Path.Combine(Config.serve_folder, data.Replace('/', '\\')));
                                return data;
                            }
                        }
                        if (newData.Length > 0) { Console.WriteLine(newData); return newData; }
                        else { return data; }
                    }
                }
            }
            return data;
        }
    }
}
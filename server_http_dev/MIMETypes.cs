﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace server_http_dev
{
    static class MIMETypes
    {
        public static Dictionary<string, string> typeMap;
        public static void init()
        {
            typeMap = new Dictionary<string, string>();

            try
            {
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.Schemas.Add(null, Config.schema_type);
                settings.ValidationType = ValidationType.Schema;
                XmlDocument doc = new XmlDocument();
                doc.Load(Config.type_map);
                XmlReader rdr = XmlReader.Create(new StringReader(doc.InnerXml), settings);
                while (rdr.Read()) { }
                loadTypeMap(doc.DocumentElement.ChildNodes);

            }
            catch (Exception e)
            {
                Console.Write(e.Message + "\r\n");
            }

        }

        static void loadTypeMap(XmlNodeList map)
        {
            if (map.Count > 0)
            {
                foreach (XmlNode match in map)
                {
                    typeMap.Add(match.Attributes.GetNamedItem("extension").Value, match.Attributes.GetNamedItem("type").Value);
                }
            }
        }
    }
}

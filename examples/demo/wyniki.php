<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="style.css" />
	<title>Wyniki</title>
</head>
<body>
	<?php 
		$todos = array(
			'studia' => array('zrobic projekt'),
			'praca' => array('wyslac maile'),
			'dom' => array('umyc okna', 'umyc psa')
		);

		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			echo "<p>Metoda: POST - dodalem wpis</p>";
			$cat = $_POST["cat"];
			$text = $_POST["text"];

			if (array_key_exists($cat, $todos)) {
				array_push($todos[$cat], $text);
			} else {
				$todos[$cat] = array($text);
			}
		} else if ($_SERVER["REQUEST_METHOD"] == "GET") {
			echo "<p>Metoda: GET - nic nie dodalem</p>";
		}
		foreach ($todos as $cat => $list) {
			if ((isset($_GET['cat']) && !empty($_GET['cat']) && $_GET['cat'] == $cat) || (!isset($_GET['cat']) || empty($_GET['cat']))) {
					echo "<div class=\"cat\">$cat <br />";

					foreach ($list as $entry) {
					 	echo "<p>$entry</p>";
					}
				}
			echo "</div>";
		}
	?>
</body>
</html>
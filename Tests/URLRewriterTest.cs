﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using NUnit.Framework;
using server_http_dev;

namespace Tests
{
    [TestFixture]
    class URLRewriterTest
    {

        [Test]
        public void RewriteTest()
        {
            Config.useDefaultSettings();

            URLRewriter.rules = new Dictionary<string, string>();
            URLRewriter.rules.Add("demo/wyniki/?$", "demo/wyniki.php");

            Assert.IsTrue(Regex.IsMatch(URLRewriter.rewrite("demo/wyniki/"), @"^demo(\\|/)wyniki[.]php$"));
            Assert.IsTrue(Regex.IsMatch(URLRewriter.rewrite("demo/wyniki"), @"^demo(\\|/)wyniki[.]php$"));
            Assert.IsFalse(Regex.IsMatch(URLRewriter.rewrite("demo/result"), @"^demo(\\|/)wyniki[.]php$"));
        }

        [Test]
        public void RegularFilesTest(){

            Config.useDefaultSettings();

            URLRewriter.rules = new Dictionary<string, string>();
            URLRewriter.rules.Add("demo/(.*)([.]php)?/?$", "demo/przepisane/$1.php");
            URLRewriter.regularFile = new List<string>();
            URLRewriter.regularFile.Add("demo/(.*)([.]php)?/?$");

            Assert.IsTrue(Regex.IsMatch(URLRewriter.rewrite("demo/wyniki.php"), @"^demo(\\|/)wyniki.php$"));
            Assert.IsTrue(Regex.IsMatch(URLRewriter.rewrite("demo/wyniki"), @"^demo(\\|/)przepisane(\\|/)wyniki[.]php$"));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace PHPInterpreter {
    [Export(typeof(server_http_dev.IInterpreter))]
    [ExportMetadata("lang", "php")]
    public class PHPInterpreter : server_http_dev.IInterpreter {
        public byte[] interpret(string path, string queryString, string postData = null) {
            if (!File.Exists(path)) {
                throw new Exception("[Interpreter - PHP] Given path does not exist!");
            }

            var p = new Process {
                // tymczasowo ścieżka absolutna
                StartInfo = new ProcessStartInfo(@"C:\php\php-cgi.exe") {
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    RedirectStandardInput = true,
                    StandardErrorEncoding = Encoding.UTF8,
                    StandardOutputEncoding = Encoding.UTF8
                }
            };
            p.StartInfo.EnvironmentVariables.Clear();
            p.StartInfo.EnvironmentVariables["REDIRECT_STATUS"] = "CGI";
            p.StartInfo.EnvironmentVariables["SCRIPT_FILENAME"] = path;
            p.StartInfo.EnvironmentVariables["GATEWAY_INTERFACE"] = "CGI/1.1";

            if (queryString.Length > 0) {
                p.StartInfo.EnvironmentVariables["QUERY_STRING"] = queryString.Substring(1);
            }
            if (postData != null) {
                p.StartInfo.EnvironmentVariables["REQUEST_METHOD"] =  "POST";
                // TODO should be set from request headers (and others)
                p.StartInfo.EnvironmentVariables["CONTENT_TYPE"] = "application/x-www-form-urlencoded";
                p.StartInfo.EnvironmentVariables["CONTENT_LENGTH"] = postData.Length.ToString();
            } else {
                p.StartInfo.EnvironmentVariables["REQUEST_METHOD"] =  "GET";
            }
            var output = new StringWriter();
            var error = new StringWriter();

            p.OutputDataReceived += (sender, args) => output.WriteLine(args.Data);
            p.ErrorDataReceived += (sender, args) => error.WriteLine(args.Data);
            p.Start();
            p.BeginOutputReadLine();
            p.BeginErrorReadLine();

            if (postData != null) {
                StreamWriter inputStreamWriter = p.StandardInput;
                inputStreamWriter.Write(postData);
                inputStreamWriter.Close();
            }
            p.WaitForExit();

            if (p.ExitCode != 0) {
                throw new Exception(string.Format(
                    "PHP failed with the following output: {0} {1}",
                    /* {0} */ Environment.NewLine,
                    /* {1} */ error.GetStringBuilder().ToString()));
            }
            p.Close();
            string res = output.GetStringBuilder().ToString();

            // Strip PHP-CGI generated headers.
            res = res.Substring(res.IndexOf("\r\n\r\n") + 4);

            return Encoding.UTF8.GetBytes(res);
        }
    }
}

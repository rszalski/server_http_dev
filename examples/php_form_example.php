<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Form Example</title>
</head>
<body>
	<form action="" method="POST"> 
		<input type="text" name="imie"></input>
		<input type="text" name="wiek"></input>
		<button type="submit">Wyslij!</button>
	</form>
	<?php 

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	echo "<p>POST</p>";

	foreach ($_POST as $key => $value) {
		echo "$key: $value <br />";
	}
} else if ($_SERVER["REQUEST_METHOD"] == "GET") {
	echo "<p>GET</p>";
}
?>

</body>
</html>
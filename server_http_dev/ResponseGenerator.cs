﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;
using System.Net;

namespace server_http_dev
{
    public class headType
    {
        public string Type;
        public string Content;
        public headType(string input)
        {
            string[] tmp = Regex.Split(input, ": ");
            Type = tmp[0];
            Content = tmp[1];
        }
    }
    public class Request
    {
        public string Method;
        public System.Uri URI;
        public string HTTPver;
        public List<headType> headers = new List<headType>();
        public string Body;
        public Request(string input)
        {
            try
            {
                string[] tmp = Regex.Split(input, "\r\n\r\n");
                Body = tmp[1];
                tmp = Regex.Split(tmp[0], "\r\n");
                char[] delimiterChars = { ' ', '\r' };
                Method = tmp[0].Split(delimiterChars)[0];
                URI = new Uri("http://" + Server.listenAddress + ":" + Server.listenPort + tmp[0].Split(delimiterChars)[1]);
                HTTPver = tmp[0].Split(delimiterChars)[2];
                for (int i = 1; i < tmp.Length; i++)
                {
                    headers.Add(new headType(tmp[i]));
                }
            }
            catch
            { throw new ArgumentException("Invalid Request"); }
        }
    }
    public class ResponseGenerator
    {
        EndPoint remoteEndPoint;
        string statusLine;
        string responseHeaders;
        byte[] content;
        bool interpret = false;
        string queryString;
        Request request;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        void getContentType(string path)
        {
            try
            {
                responseHeaders += "Content-Type: " + MIMETypes.typeMap[path] +"\r\n"+ "; charset=utf-8\r\n";
                log.Debug("Rozpoznany typ: " + MIMETypes.typeMap[path]);
            }
            catch
            {
                log.Debug("unknown content type");
                responseHeaders += "Content-Type: text/html; charset=utf-8\r\n";
            }
            if(Server.interpreter.interpretedTypes.Contains(path))
                interpret = true;
        }
        public bool getContent(Uri u)
        {
            string path = u.AbsolutePath;
            queryString = u.Query;
            char[] tc = {'/'};
            if (path == "/")
            {
                path = "index.htm";
            }
            else
            {
                path = path.Trim(tc);
            }
            log.Debug("sciezka bazowa: " + path);
            path = Path.Combine(Config.serve_folder, URLRewriter.rewrite(path));

            // If rewrte rewrote the URL to one with query string, we need to clean it before passing to security
            Uri tempUri = new Uri("http://bullshit/" + path);

            string securePath = tempUri.AbsolutePath;
            securePath = securePath.Trim(tc); // trim '/' from the beggining
            // remember to preserve new query string
            queryString = tempUri.Query;
            log.Debug("sciezka po podmianie: " + path);

            if (System.IO.File.Exists(securePath) && Security.checkPath(securePath))
            {               
                getContentType(System.IO.Path.GetExtension(securePath));
                if (interpret)
                {
                    content = Server.interpreter.interpret(Path.GetFullPath(securePath), Path.GetExtension(securePath).Substring(1), queryString);
                }
                else
                {
                    this.content = System.IO.File.ReadAllBytes(path);
                }
                return (true);
            }
            else
                return (false);
        }
        bool postContent(Uri u)
        {
            string path = u.AbsolutePath;
            queryString = u.Query;
            char[] tc = { '/' };
            path = path.Trim(tc);   //obcięcie początowego '/'
            path = Path.Combine(Config.serve_folder, URLRewriter.rewrite(path));
            log.Debug("Pełna ścieżka: " + Path.GetFullPath(path));
            if(true) //(System.IO.File.Exists(path) && Security.checkPath(path))
            {
                try
                {
                    content = Server.interpreter.interpret(Path.GetFullPath(path), Path.GetExtension(path).Substring(1), queryString, request.Body);
                }
                catch
                {
                    return false;
                }
            }

            return true;
        }
        public byte[] generate(string input, EndPoint remoteEndPoint)
        {
            this.remoteEndPoint = remoteEndPoint;
            request = new Request(input);
            if (Security.hasAccess(((IPEndPoint)this.remoteEndPoint).Address))
            {
                switch (request.Method)
                {
                    case "GET":
                        if (!getContent(request.URI))
                        {
                            //nie znaleziono pliku: 404
                            statusLine = "HTTP/1.1 404\r\n";
                            content = Encoding.UTF8.GetBytes("404 File not found");
                            responseHeaders = "Server: sfi\r\nContent-Type: text/html; charset=utf-8\r\nAccept-Ranges: bytes\r\nConnection: close\r\nContent-Length: " + content.Length + "\r\n";
                            break;
                        }
                        statusLine = "HTTP/1.1 200\r\n";
                        responseHeaders += "Server: sfi\r\nAccept-Ranges: bytes\r\nConnection: close\r\nContent-Length: " + content.Length + "\r\n";
                        break;
                    case "POST":
                        if (!postContent(request.URI))
                        {
                            statusLine = "HTTP/1.1 204\r\n";
                            content = Encoding.UTF8.GetBytes("204 No content");
                            responseHeaders = "Server: sfi\r\nContent-Type: text/html\r\nAccept-Ranges: bytes\r\nConnection: close\r\nContent-Length: " + content.Length + "\r\n";
                            break;
                        }
                        statusLine = "HTTP/1.1 200\r\n";
                        responseHeaders += "Server: sfi\r\nAccept-Ranges: bytes\r\nConnection: close\r\n";
                        if (content != null)
                        {
                            responseHeaders += "Content-Type: text/html; charset=utf-8\r\nContent-Length: " + content.Length + "\r\n";
                        }
                        break;
                }
            }
            else
            {
                content = Encoding.UTF8.GetBytes("403 Forbidden");
                responseHeaders = "Server: sfi\r\nContent-Type: text/html; charset=utf-8\r\nAccept-Ranges: bytes\r\nConnection: close\r\nContent-Length: " + content.Length + "\r\n";
                statusLine = "HTTP/1.1 403\r\n";
            }
            log.Info(request.Method + " | " + request.URI.AbsolutePath + " | " + statusLine);
            log.Debug("ODPOWIEDZ: " + statusLine + responseHeaders + "\r\n");
            byte[] tmp = Encoding.UTF8.GetBytes( statusLine + responseHeaders + "\r\n");
            if (content != null)
            {
                byte[] ret = new byte[tmp.Length + content.Length];
                System.Buffer.BlockCopy(tmp, 0, ret, 0, tmp.Length);
                System.Buffer.BlockCopy(content, 0, ret, tmp.Length, content.Length);
                return (ret);
            }         
            return (tmp);
        }

    }
}

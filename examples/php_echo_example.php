<!DOCTYPE html>
<html lang="pl">
  <head>
    <meta charset="UTF-8" />
	<title>Echo Example</title>
</head>
<body>
<?php 
	
	echo "Zmienne w tablicy \$_GET <br />";

	foreach ($_GET as $key => $value) {
		echo "$key: $value <br />";
	}

	echo "Zmienne w tablicy \$_POST <br />";

	print_r($_POST);

	foreach ($_POST as $key => $value) {
		echo "$key: $value <br />";
	}
	echo "Zmienne w tablicy \$_SERVER <br />";

	foreach ($_SERVER as $key => $value) {
		echo "$key: $value <br />";
	}
?>
</body>
</html>
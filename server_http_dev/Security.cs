﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading.Tasks;
using System.Net;
using log4net;

namespace server_http_dev
{
    public static class Security
    {
        public static List<IPAddress> ipList;
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
       
        public const ushort FILTERING_NONE = 0;
        public const ushort FILTERING_BLACKLIST = 1;
        public const ushort FILTERING_WHITELIST = 2;

        public static bool checkPath(String path)
        {
            return Path.GetFullPath(path).Contains(Config.serve_folder);
        }

        public static void loadIpList()
        {
            XmlTextReader reader = new XmlTextReader(Config.ip_list);

            reader.WhitespaceHandling = WhitespaceHandling.None;
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(reader);
            reader.Close();

            getIps(xmlDoc.GetElementsByTagName("ipaddress"));
        }

        private static void getIps(XmlNodeList xmlIps){
            if (xmlIps.Count > 0)
            {
                XmlNode enabled = null;
                IPAddress value;
                foreach (XmlNode ip in xmlIps)
                {
                    enabled = ip.Attributes.GetNamedItem("enabled");
                    if (enabled == null || enabled.Value.ToUpper().Equals("TRUE"))
                    {
                        if (Security.ipList == null)
                        {
                            Security.ipList = new List<IPAddress>();
                        }
                        try
                        {
                            value = IPAddress.Parse(ip.Attributes.GetNamedItem("value").Value);
                            Security.ipList.Add(value);
                        }
                        catch (FormatException e)
                        {
                            log.Info(e.Message + " File:" + Config.ip_list + " !");
                        }
                    }
                }
                if (Security.ipList.Count == 0)
                {
                    log.Warn("File " + Config.ip_list + "does not contains correctly IP addresses!");
                }
            }
            else
            {
                log.Warn("File " + Config.ip_list + "does not contains correctly IP addresses!");
            }
        }

        public static bool hasAccess(IPAddress ip)
        {
            if (Config.ip_filtering == Security.FILTERING_NONE ||
                (Config.ip_filtering == Security.FILTERING_BLACKLIST && !Security.ipList.Contains(ip)) ||
                (Config.ip_filtering == Security.FILTERING_WHITELIST && Security.ipList.Contains(ip)))
                return true;
            return false;
        }
    }
}

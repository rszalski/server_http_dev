﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

namespace server_http_dev
{
    public static class Config
    {
        public static ushort listen_port { private set; get; }
        public static IPAddress listen_address { private set; get; }
        public static String config_file { private set; get; }
        public static String serve_folder { private set; get; }
        public static String log_level { private set; get; }
        public static Boolean verbose { private set; get; }
        public static String schema_rules { private set; get; }
        public static String rewrite_rules { private set; get; }
        public static String type_map { private set; get; }
        public static String schema_type { private set; get; }
        public static ushort ip_filtering { private set; get; }
        public static String ip_list { private set; get; }

        public static void loadSettings(string[] args)
        {

            using (StreamReader reader = File.OpenText(@"../../../server_http_dev/config/config.json"))
            {
                JObject jsonConfig = (JObject)JToken.ReadFrom(new JsonTextReader(reader));

                listen_port = Convert.ToUInt16(jsonConfig.GetValue("listen_port").ToString());
                listen_address = IPAddress.Parse(jsonConfig.GetValue("listen_address").ToString());
                config_file = Path.GetFullPath(jsonConfig.GetValue("config_file").ToString());
                serve_folder = Path.GetFullPath(jsonConfig.GetValue("serve_folder").ToString());
                schema_rules = Path.GetFullPath(jsonConfig.GetValue("schema_rules").ToString());
                rewrite_rules = Path.GetFullPath(jsonConfig.GetValue("rewrite_rules").ToString());
                type_map = Path.GetFullPath(jsonConfig.GetValue("type_map").ToString());
                schema_type = Path.GetFullPath(jsonConfig.GetValue("schema_type").ToString());
                log_level = jsonConfig.GetValue("log_level").ToString();
                ip_filtering = Convert.ToUInt16(jsonConfig.GetValue("ip_filtering").ToString());
                if (ip_filtering != 0)
                {
                    ip_list = Path.GetFullPath(jsonConfig.GetValue("ip_list").ToString());
                    Security.loadIpList();
                }
                verbose = false;

                setTempSettings(args);

            }
        }

        public static void useDefaultSettings()
        {
            loadSettings(new String[0]);
        }

        public static bool setSetting(String setting, String value)
        {
            try
            {
                switch (setting)
                {
                    case "p":
                    case "listen_port":
                        listen_port = Convert.ToUInt16(value);
                        break;
                    case "a":
                    case "listen_address":
                        listen_address = IPAddress.Parse(value);
                        break;
                    case "c":
                    case "config_file":
                        if (File.Exists(Path.GetFullPath(value)))
                        {
                            config_file = Path.GetFullPath(value);
                        } else { return false; }
                        break;
                    case "s":
                    case "serve_folder":
                        if (File.Exists(Path.GetFullPath(value)))
                        {
                            serve_folder = Path.GetFullPath(value);
                        }
                        else { return false; }
                        break;
                    case "l":
                    case "log_level":
                        log_level = value;
                        break;
                    case "v":
                    case "verbose":
                        if (value.ToUpper().Equals("TRUE")) verbose = true;
                        else if (value.ToUpper().Equals("FALSE")) verbose = false;
                        else throw new Exception();
                        break;
                    case "sr":
                    case "schema_rules":
                        if (File.Exists(Path.GetFullPath(value)))
                        {
                            schema_rules = Path.GetFullPath(value);
                        }
                        else { return false; }
                        break;
                    case "r":
                    case "rewrite_rules":
                        if (File.Exists(Path.GetFullPath(value)))
                        {
                            rewrite_rules = Path.GetFullPath(value);
                        }
                        else { return false; }
                        break;
                    case "h":
                    case "help":
                        help();
                        break;
                    case "tm":
                    case "type_map":
                        if (File.Exists(Path.GetFullPath(value)))
                        {
                            type_map = Path.GetFullPath(value);
                        }
                        else { return false; }
                        break;
                    case "st":
                    case "schema_type":
                        if (File.Exists(Path.GetFullPath(value)))
                        {
                            schema_type = Path.GetFullPath(value);
                        }
                        else { return false; }
                        break;
                    case "f":
                    case "ip_filtering":
                        ip_filtering = Convert.ToUInt16(value);
                        break;
                    case "il":
                    case "ip_list":
                        if (File.Exists(Path.GetFullPath(value)))
                        {
                            ip_list = Path.GetFullPath(value);
                        }
                        else { return false; }
                        break;
                    default:
                        Console.WriteLine("Setting '{0}' not found!\n", setting);
                        help();
                        return false;
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Incorrect value {0} for {1}\n\n", value, setting);
                help();
                return false;
            }
            return true;
        }

        private static void setTempSettings(String[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                if (Regex.IsMatch(args[i], "^-.*"))
                {
                    String[] tmpSetting = null;
                    if (Regex.IsMatch(args[i], "^--.*"))
                    {
                        tmpSetting = args[i].Substring(2, args[i].Length - 2).Split('=');
                    }
                    else
                    {
                        tmpSetting = args[i].Substring(1, args[i].Length - 1).Split('=');
                    }
                    switch (tmpSetting.Length)
                    {
                        case 1:
                            if ((i == args.Length - 1) || (Regex.IsMatch(args[i + 1], "^-.*")))
                            {
                                if (!setSetting(tmpSetting[0], "TRUE")) return;
                            }
                            else
                            {
                                if (!setSetting(tmpSetting[0], args[i + 1])) return;
                                i++;
                            }
                            break;
                        case 2:
                            if (!setSetting(tmpSetting[0], tmpSetting[1])) return;
                            break;
                        default:
                            return;
                    }
                }
                else return;
            }
        }

        public static bool hasHelp(String[] args)
        {
            foreach (string x in args)
            {
                if (Regex.IsMatch(x, "^-(-)?h(elp)?$"))
                {
                    setSetting("help", "");
                    return true;
                }
            }
            return false;
        }

        public static void help()
        {
            Console.Write("========> HELP <========\n" +
                          "| -p   listen_port     |\n" +
                          "| -a   listen_address  |\n" +
                          "| -l   log_level       |\n" +
                          "| -s   serve_folder    |\n" +
                          "| -c   config_file     |\n" +
                          "| -v   verbose         |\n" +
                          "| -r   rewrite_rules   |\n" +
                          "| -sr  schema_rules    |\n" +
                          "| -h   help            |\n" +
                          "| -tm  type_map        |\n" +
                          "| -st  schema_type     |\n" +
                          "| -f   ip_filtering    |\n" +
                          "| -il  ip_list         |\n" +
                          "========================\n");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using NUnit.Framework;
using server_http_dev;

namespace Tests
{
    [TestFixture]
    public class ConfigTest
    {
        [Test]
        public void LoadingSettingFromFileTest()
        {
            Config.useDefaultSettings();

            Assert.IsNotNull(Config.config_file);
            Assert.IsNotNull(Config.listen_address);
            Assert.IsNotNull(Config.log_level);
            Assert.IsNotNull(Config.rewrite_rules);
            Assert.IsNotNull(Config.schema_rules);
            Assert.IsNotNull(Config.schema_type);
            Assert.IsNotNull(Config.serve_folder);
            Assert.IsNotNull(Config.type_map);
            if (Config.ip_filtering == 0) Assert.IsNull(Config.ip_list);
            else Assert.IsNotNull(Config.ip_list);
        }

        [Test]
        public void DetectionHelpOptionTest1()
        {
            Config.useDefaultSettings();

            String[] args = { "-h" };
            Assert.IsTrue(Config.hasHelp(args));
            args[0] = "--h";
            Assert.IsTrue(Config.hasHelp(args));
            args[0] = "-help";
            Assert.IsTrue(Config.hasHelp(args));
            args[0] = "--help";
            Assert.IsTrue(Config.hasHelp(args));
        }

        [Test]
        public void DetectionHelpOptionTest2()
        {
            Config.useDefaultSettings();

            String[] args = { "h" };
            Assert.IsFalse(Config.hasHelp(args));
            args[0] = "help";
            Assert.IsFalse(Config.hasHelp(args));
            args[0] = "hlp";
            Assert.IsFalse(Config.hasHelp(args));
            args[0] = "- help";
            Assert.IsFalse(Config.hasHelp(args));
        }

        [Test]
        public void SetTempSettingTest()
        {
            Assert.IsFalse(Config.setSetting("s", "../mojWymyslonyFolder"));
            Assert.IsFalse(Config.setSetting("wymyslone_ustawienie", "../mojWymyslonyFolder"));
            Assert.IsTrue(Config.setSetting("s", "../../../server_http_dev/config/config.json"));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;

namespace server_http_dev {
    public interface IInterpreter {
        byte[] interpret(string path, string queryString, string postData = null);
    }

    public interface IInterpreterData {
        string lang { get; }
    }

    class Interpreter {
        [ImportMany]
        private IEnumerable<Lazy<IInterpreter, IInterpreterData>> interpreters;

        private CompositionContainer _container;
        public HashSet<string> interpretedTypes { private set; get; }

        public Interpreter() {
            this.interpretedTypes = new HashSet<string>();

            AggregateCatalog catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new DirectoryCatalog("../../../interpreters"));
            this._container = new CompositionContainer(catalog);

            try {
                this._container.ComposeParts(this);

                foreach (Lazy<IInterpreter, IInterpreterData> interpreter in this.interpreters) {
                    Console.WriteLine("Found interpreter for {0}", interpreter.Metadata.lang);
                    this.interpretedTypes.Add("." + interpreter.Metadata.lang);
                }
            } catch (CompositionException ex) {
                Console.WriteLine("Error while composing! " + ex.ToString());
            }
        }

        public byte[] interpret(string path, string lang, string queryString, string postData = null) {
            foreach (Lazy<IInterpreter, IInterpreterData> interpreter in this.interpreters) {
                if (interpreter.Metadata.lang.Equals(lang)) {
                    return interpreter.Value.interpret(path, queryString, postData);
                }
            }
            throw new Exception(string.Format("No suitable interpreter found for language: {0}", lang));
        }
    }
}

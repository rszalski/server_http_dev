﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace server_http_dev {
    class Server {
        public static Interpreter interpreter;
        public static int listenPort { private set; get; }
        public static IPAddress listenAddress { private set; get; }
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void Main(string[] args) {
            try {
                if (!Config.hasHelp(args)) {
                    Config.loadSettings(args);
                    URLRewriter.init();
                    MIMETypes.init();
                    Server.interpreter = new Interpreter();
                    log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(@"../../config/logger.config"));
                    Server.startListening();
                }
            } catch (Exception ex) {
                log.Warn(ex.Message);
            }
        }

        private static ManualResetEvent allDone = new ManualResetEvent(false);
        
        public static void startListening() {
            Server.listenPort = Config.listen_port;
            Server.listenAddress = Config.listen_address;
            IPEndPoint ipEndPoint = new IPEndPoint(Server.listenAddress, Server.listenPort);

            log.Info("Server is listening on " + Server.listenAddress.ToString() +":" + Server.listenPort);

            Socket listener = new Socket(ipEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            try {
                listener.Bind(ipEndPoint);
                listener.Listen(10);

                while (true) {
                    Server.allDone.Reset();

                    listener.BeginAccept(new AsyncCallback(Server.acceptCallback), listener);

                    Server.allDone.WaitOne();
                }
            } catch (Exception e) {
                log.Warn(e.ToString());
            }
            log.Info("Closing listener...");
        }

        private static void acceptCallback(IAsyncResult result) {
            // Signal the main thread to continue.
            Server.allDone.Set();

            Socket listener = (Socket) result.AsyncState;
            Socket handler = listener.EndAccept(result);
            log.Info("Accepted a connection from " + handler.RemoteEndPoint.ToString());

            // Create the state object.
            SocketState state = new SocketState();
            state.workSocket = handler;
            handler.BeginReceive(state.buffer, 0, SocketState.bufferSize, 0,
                new AsyncCallback(Server.readCallback), state);
        }

        public static void readCallback(IAsyncResult result) {
            SocketState state = (SocketState) result.AsyncState;
            log.Debug("GOT: " + Encoding.ASCII.GetString(state.buffer));
            Socket handler = state.workSocket;

            // Read data from the browser socket.
            int read = handler.EndReceive(result);

            // Data was read from the browser socket.
            if (read > 0) {
                // Sends requested content or 404 error
                ResponseGenerator rg = new ResponseGenerator();
                Server.send(handler, rg.generate(Encoding.ASCII.GetString(state.buffer), handler.RemoteEndPoint));

                state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, read));
                try {
                    handler.BeginReceive(state.buffer, 0, SocketState.bufferSize, 0,
                        new AsyncCallback(readCallback), state);
                } catch (Exception ex) {
                    // pokemon, Łapie error połączenie przerwane na komputerze hoście
                }
            } else {
                handler.Close();
            }
        }

        private static void send(Socket browser, String data) {
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.
            browser.BeginSend(byteData, 0, byteData.Length, SocketFlags.None,
                new AsyncCallback(sendCallback), browser);
        }

        private static void send(Socket browser, byte[] data)
        {
            // Begin sending the data to the remote device.
            browser.BeginSend(data, 0, data.Length, SocketFlags.None,
                new AsyncCallback(sendCallback), browser);
        }

        private static void sendCallback(IAsyncResult result) {
            try {
                // Retrieve the socket from the state object.
                Socket browser = (Socket) result.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = browser.EndSend(result);
                log.Debug("Sent " + bytesSent +" bytes to the Browser.");
            } catch (Exception e) {
                //log.Warn(e.ToString());
            }
        }
    }
}
